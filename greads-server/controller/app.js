var express = require('express');
var app = express();

var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var jsonParser = bodyParser.json();

var path = require('path');

var cors = require("cors")
var cor = cors();
app.use(cor);
app.use(express.static(path.join(__dirname, "../public")));

var library = require('../model/library.js');

app.get('/api/library/category/:cat', function (req, res) {
    var cat = req.params.cat;
    library.getContentbyCat(cat, function (err, result) {
        if (!err) {
            res.send(result);
        }
        else {
            console.log(err);
            res.status(500).send(err);
        }
    });
});

app.get('/api/library', function (req, res) {
    library.getContent(function (err, result) {
        if (!err) {
            res.send(result);
        }
        else {
            console.log(err);
            res.status(500).send(err);
        }
    });
});

app.post('/api/library', urlencodedParser, jsonParser, function (req, res) {
    var title = req.body.title;
    var author = req.body.author;
    var datecreated = req.body.datecreated;
    var img = req.body.img;
    var cat = req.body.cat;
    var post = req.body.post;

    library.addContent(title, author, datecreated, img, cat, post, function (err, result) {
        if (!err) {
            console.log(result);
            res.send(result.affectedRows + ' Content Added');
        }
        else {  
            console.log(err);
            res.status(500).send(err.code);
        }
    })
})


app.delete('/api/library/:id', function (req, res) {
    var id = req.params.id;

    library.deleteContent(id, function (err, result) {
        if (!err) {
            console.log(result);
            res.send(result.affectedRows + ' Content Removed');
        }
        else {
            console.log(err);
            res.status(500).send(err.code);
        }
    })
})


app.post('/api/library/:id', urlencodedParser, jsonParser, function (req, res) {
    var title = req.body.title;
    var author = req.body.author;
    var datecreated = req.body.datecreated;
    var img = req.body.img;
    var cat = req.body.cat;
    var post = req.body.post;
    var id = req.params.id;

    library.updateContent(title, author, datecreated, img, cat, post, id, function (err, result) {
        if (!err) {
            console.log(result);
            res.send(result.affectedRows + ' Content Updated');
        }
        else {
            console.log(err);
            res.status(500).send(err.code);
        }
    })
})

module.exports = app