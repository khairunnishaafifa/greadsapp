import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddContentPage } from './add-content.page';

describe('AddContentPage', () => {
  let component: AddContentPage;
  let fixture: ComponentFixture<AddContentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddContentPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddContentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
