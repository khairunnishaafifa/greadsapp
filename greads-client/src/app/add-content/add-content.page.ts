import { Component, OnInit } from '@angular/core';
import { LibraryService } from '../library.service';
import { ModalController } from '@ionic/angular';
import { Response } from '@angular/http';

@Component({
  selector: 'app-add-content',
  templateUrl: './add-content.page.html',
  styleUrls: ['./add-content.page.scss'],
})
export class AddContentPage implements OnInit {
  library: any;
  data = {
    title: '',
    author: '',
    datecreated: '',
    img: '',
    cat: '',
    post: ''
  };

  constructor(
    public libraryservice: LibraryService,
    public modalController: ModalController
  ) { }

  ngOnInit() {
  }

  addContent() {
    console.log(this.data);
    this.libraryservice.addContent(this.data).subscribe((response: Response) => {
      if (response) {
        this.libraryservice.message('Data Saved');
      } else {
        this.libraryservice.message('Something Wrong!');
      }
      this.modalController.dismiss();
    });
  }

  changeListener($event): void {
    this.readThis($event.target);
  }

  readThis(inputValue: any): void {
    var file: File = inputValue.files[0];
    var myReader: FileReader = new FileReader();

    myReader.onloadend = (e) => {
      const myimg: string = myReader.result as string;
      this.data.img = myimg;

      console.log(this.data.img);
    }
    myReader.readAsDataURL(file);
  }

  cancel() {
    this.modalController.dismiss();
  }
}
