import { Component } from '@angular/core';
import { LibraryService } from '../library.service';
import { ModalController, ActionSheetController, ToastController } from '@ionic/angular';
import { Response } from '@angular/http';
import { UpdateContentPage } from '../update-content/update-content.page';
import { AddContentPage } from '../add-content/add-content.page';
import { DetailContentPage } from '../detail-content/detail-content.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  categorySelected: any;
  LibraryList: any;

  sliderConfig = {
    slidesPerView: 1,
    spaceBetween: 10,
    centeredSlides: true
  };

  constructor(
    public libraryservice: LibraryService,
    public modalcontroller: ModalController,
    public toastcontroller: ToastController,
    public actionSheetController: ActionSheetController
  ) {
    this.categorySelected = "1";
    this.loadLibrary();
  }

  loadLibrary() {
    this.libraryservice.loadLibrary(this.categorySelected).subscribe((response: Response) => {
      let data = response.json();
      for (let elem of data) {
        elem.img = elem.img.split(',');
      }
      this.LibraryList = data;
    });
  }

  async goDetailContent(data) {
    const modal = await this.modalcontroller.create({
      component: DetailContentPage,
      componentProps: { value: data }
    });
    modal.onDidDismiss().then(() => { this.loadLibrary() });
    return await modal.present();
  }

  async goUpdateContent(data) {
    const modal = await this.modalcontroller.create({
      component: UpdateContentPage,
      componentProps: { value: data }
    });
    modal.onDidDismiss().then(() => { this.loadLibrary() });
    return await modal.present();
  }

  async goAddContent() {
    const modal = await this.modalcontroller.create({
      component: AddContentPage,
    });
    modal.onDidDismiss().then(() => { this.loadLibrary() });
    return await modal.present();
  }

  deleteContent(id) {
    this.libraryservice.deleteContent(id).subscribe((response: Response) => {
      if (response.ok) {
        this.presentToastDelete();
      }
      this.loadLibrary();
    })
  }

  async presentToastDelete() {
    const toast = await this.toastcontroller.create({
      message: 'Content Removed',
      color: "dark",
      duration: 2000
    });
    toast.present();
  }
}
