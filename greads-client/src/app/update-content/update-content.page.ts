import { Component, OnInit } from '@angular/core';
import { NavParams, NavController, ModalController, ToastController } from '@ionic/angular';
import { LibraryService } from '../library.service';
import { Response } from '@angular/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-update-content',
  templateUrl: './update-content.page.html',
  styleUrls: ['./update-content.page.scss'],
})
export class UpdateContentPage implements OnInit {
  library: any;
  data = {
    id: '',
    title: '',
    author: '',
    datecreated: '',
    img: '',
    cat: '',
    post: ''
  };

  constructor(
    public navParams: NavParams,
    public navCtrl: NavController,
    public libraryservice: LibraryService,
    public modalcontroller: ModalController,
    public router: Router,
    public toastcontroller: ToastController
  ) {
    this.library = this.navParams.data.value;
    console.log(this.library);
    this.data.id = this.library.id;
    this.data.title = this.library.title;
    this.data.author = this.library.author;
    this.data.datecreated = this.library.datecreated;
    this.data.img = this.library.img;
    this.data.cat = this.library.cat;
    this.data.post = this.library.post;
    console.log(this.data);
  }

  ngOnInit() {
  }

  updateContent() {
    console.log(this.library);
    this.libraryservice.updateContent(this.library).subscribe((response: Response) => {
      if (response) {
        this.libraryservice.message('Data Saved');
      } else {
        this.libraryservice.message('Something Wrong!');
      }
      this.modalcontroller.dismiss();
    });
  }

  changeListener($event): void {
    this.readThis($event.target);
  }

  readThis(inputValue: any): void {
    var file: File = inputValue.files[0];
    var myReader: FileReader = new FileReader();

    myReader.onloadend = (e) => {
      const myimg: string = myReader.result as string;
      this.library.img = myimg;

      console.log(this.library.img);
    }
    myReader.readAsDataURL(file);
  }

  deleteContent(id) {
    this.libraryservice.deleteContent(id).subscribe((response: Response) => {
      if (response.ok) {
        this.presentToastDelete();
      }
      this.modalcontroller.dismiss();
    })
  }

  async presentToastDelete() {
    const toast = await this.toastcontroller.create({
      message: 'Content Removed',
      color: "dark",
      duration: 2000
    });
    toast.present();
  }
}