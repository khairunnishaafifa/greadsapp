import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LibraryService {

  constructor(public http: Http, public toastController: ToastController) { }

  loadLibrary(cat) {
    return this.http.get('http://localhost:8081/api/library/category/' + cat);
  }

  getContent() {
    return this.http.get('http://localhost:8081/api/library/');
  }

  addContent(data) {
    return this.http.post('http://localhost:8081/api/library/', data);
  }

  updateContent(data) {
    return this.http.post('http://localhost:8081/api/library/' + data.id, data);
  }

  deleteContent(id) {
    console.log(id);
    return this.http.delete('http://localhost:8081/api/library/' + id);
  }

  async message(msg) {
    const toast = await this.toastController.create({
      color: "dark",
      message: msg,
      duration: 2000
    });
    toast.present();

  }
}

