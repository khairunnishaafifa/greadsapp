import { Component, OnInit } from '@angular/core';
import { LibraryService } from '../library.service';
import { ModalController, ActionSheetController, ToastController, NavParams, NavController } from '@ionic/angular';
import { UpdateContentPage } from '../update-content/update-content.page';


@Component({
  selector: 'app-detail-content',
  templateUrl: './detail-content.page.html',
  styleUrls: ['./detail-content.page.scss'],
})
export class DetailContentPage implements OnInit {
  library: any;
  data = {
    id: '',
    title: '',
    author: '',
    datecreated: '',
    img: '',
    cat: '',
    post: ''
  };

  constructor(
    public navParams: NavParams,
    public navCtrl: NavController,
    public libraryservice: LibraryService,
    public modalcontroller: ModalController,
    public toastcontroller: ToastController,
    public actionSheetController: ActionSheetController
  ) {
    this.library = this.navParams.data.value;
    console.log(this.library);
    this.data.id = this.library.id;
    this.data.title = this.library.title;
    this.data.author = this.library.author;
    this.data.datecreated = this.library.datecreated;
    this.data.img = this.library.img;
    this.data.cat = this.library.cat;
    this.data.post = this.library.post;
    console.log(this.data);
  }

  ngOnInit() {
  }

  async goUpdateContent(library) {
    const modal = await this.modalcontroller.create({
      component: UpdateContentPage,
      componentProps: { value: library }
    });
    modal.onDidDismiss().then(() => { this.back() });
    return await modal.present();
  }

  back() {
    this.modalcontroller.dismiss();
  }
}
