import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'update-content', loadChildren: './update-content/update-content.module#UpdateContentPageModule' },
  { path: 'detail-content', loadChildren: './detail-content/detail-content.module#DetailContentPageModule' },
  { path: 'add-content', loadChildren: './add-content/add-content.module#AddContentPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
